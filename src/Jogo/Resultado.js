import React, {Component} from 'react'
import {List, Container} from 'semantic-ui-react'

import Resposta from "./Resposta"
import Navegacao from './Navegacao'


class Resultado extends Component {
    render(){

        const perguntasRespostas = this.props.history.location.state.resultado
        
        return (
            <div>
                <Navegacao/>
                <h2>Seus Resultados</h2>
                <p>Confira o seu desempenho nesta categoria!</p>
                <Container>
                    <List divided verticalAlign="left"> 
                {
                    Object.keys(perguntasRespostas)
                        .map(key => {
                            let pergunta = perguntasRespostas[key].pergunta
                            let resposta = perguntasRespostas[key].resposta
                            let acertou = perguntasRespostas[key].acertou
                            console.log(pergunta);
                            console.log(resposta);
                            console.log(acertou);
                            return <Resposta resposta={resposta} pergunta={pergunta} key={key}/>
                            
                        })
                }                   

                    </List>
                </Container>
            </div>
        )
    }
}

export default Resultado