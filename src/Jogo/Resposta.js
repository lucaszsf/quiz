import React from 'react'
import {List, Label, Icon} from 'semantic-ui-react'

const Resposta = props => {
    const {pergunta, resposta, key} = props
    return (
        <List.Item key={key}>
            <List.Content floated="left">
                <Label>
                    {pergunta}
                </Label>
            </List.Content>                       
            <List.Content floated="right">
                {resposta}
                <Icon name="thumbs up outline" />
            </List.Content>
        </List.Item>
    )
}
// thumbs up outline
// thumbs down outline
export default Resposta